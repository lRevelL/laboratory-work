#include <iostream>
#include <string>
using namespace std;

void check_path(string);
string return_file_name(string,bool);
string merge_path(string,string);
bool path_is_file(string);

int main(){
	
	string path = "F:\\programs\\elite_dangerous.exe";
	check_path(path);
	cout << endl;
	
	string file1 = return_file_name(path,false);
	cout << file1 << endl;
	
	string file2 = return_file_name(path,true);
	cout << file2 << "\n" << endl;
	
	string path1 = "E:\\Users";
	string path2 = "\\Alexey\\GG\\ED.png";
	string path3 = merge_path(path1,path2);
	cout << path3 << endl;
	
	string path4 = "A:\\user\\azino777\\setup.exe";
	if (path_is_file(path4)){
		cout << "Path is file";
	}
	else{
		cout << "Path is not a file";
	}
	return 0;
}

void check_path(string path){
	int substr_flag = 0;
		int delta = 0;
		for (int i = 0; i < path.size(); i++){
			if ((path[i] == '\\') && (path[i-1] == ':')){
				substr_flag = i+1;
				cout << "-Found disk: " << path.substr(0,i) << endl;
			}
			else if (path[i] == '\\' && (i - substr_flag) != 0){
				delta = i - substr_flag;
				cout << "--Found folder: " << path.substr(substr_flag,delta) << endl;
				substr_flag = i+1;
			}
			else if (path[i] == '.'){
				delta = path.size() - substr_flag;
				cout << "---Found file: " << path.substr(substr_flag,delta) << endl;
				substr_flag = i;
			}
		}
}

string return_file_name(string path,bool full_name = true){
	int substr_flag = 0;
	int delta = 0;
	string file_name = "";
	int length_file_extention = 0;
	for (int i = 0; i < path.size(); i++){
		if (path[i] == '\\')
			substr_flag = i+1;
		else if (path[i] == '.'){
			delta = path.size() - substr_flag;
			length_file_extention = delta - (path.size() - i);
			file_name = path.substr(substr_flag,delta);
			if(!full_name)
			file_name = file_name.substr(0,length_file_extention);
		}
	}
	return file_name;
	
}

string merge_path(string path1,string path2){
	return path1+path2;
}

bool path_is_file(string path){
	for (int i; i < path.size(); i++){
		if (path[i] == '.'){
			return true;
		}
	}
}