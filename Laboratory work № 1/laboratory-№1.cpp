#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Path {
	vector<string> path_elements;
	const string EXCEPTIONS = "*/?<>";

	bool path_has_disk = false;
	bool path_has_file = false;
	int length_file_extention = 0;

public:
	Path(string path) {
		try{
			if(path_has_exceptions(path)){
				throw 1;
			}
				check_path(path);
		}
			catch (int ex){
				cout << "\nError: " << ex << endl;
				cout << "Cant read path - " << path  << endl;
			}
	}

private:
	
	bool path_has_exceptions(string path){
		int i = 0;
		while (i < path.length()){
			i ++;
			if (path_has_exception_symbol(path[i])) {
				return true;
			}
		}
		return false;
	}
	bool path_has_exception_symbol(char symbol) {
		for (int i = 0; i < EXCEPTIONS.size(); i++) {
			if (symbol == EXCEPTIONS[i]) {
				return true;
			}
		}
		return false;
	}

	void check_path(string path) {
		int substr_flag = 0;
		int delta = 0;
		
			for (int i = 0; i < path.length(); i++) {
				if ((path[i] == '\\') && (path[i - 1] == ':') && (path_elements.size() == 0)) {

					path_has_disk = true;

					substr_flag = i + 1;
					path_elements.push_back(path.substr(0, i));
				}
				else if (path[i] == '\\' && (i - substr_flag) != 0) {
					delta = i - substr_flag;
					path_elements.push_back(path.substr(substr_flag, delta));
					substr_flag = i + 1;
				}
				else if (path[i] == '.') {
					delta = path.size() - substr_flag;

					length_file_extention = delta - (path.size() - i);
					path_has_file = true;

					path_elements.push_back(path.substr(substr_flag, delta));
					substr_flag = i;
				}
			}
			if (path_elements.size() == 0) {
				cerr << "Can\'t read >> " << path << " << path.";
			}
	}


	string build_path() {
		string path = "";
		for (int i = 0; i < path_elements.size(); i++) {
			if (i != 0) {
				path += "\\";
			}
			path += path_elements[i];
		}
		return path;
	}
	
public:
	string return_file_name(bool full_name = true) { //false - для вывода названия файла без расширения 
		return (path_has_file) && (full_name) ? path_elements[path_elements.size() - 1] : path_has_file ? path_elements[path_elements.size() - 1].substr(0, length_file_extention) : "";
	}

	string return_path_element(int element_index) {
		return path_elements[element_index];
	}

	string return_path() {
		return build_path();
	}
	bool path_is_file(){
		if(path_has_file)
			return true;
		else
			return false;
		
	}
	int return_size_element() {
		return path_elements.size();
	}

	Path merge_path(Path path1, Path path2) {
		string new_path = "";

		for (int i = 0; i < path1.path_elements.size(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += path1.path_elements[i];
		}
		for (int i = 0; i < path2.path_elements.size(); i++) {
			new_path += "\\";
			new_path += path2.path_elements[i];
		}
		return new_path;
	}

	string merge_path(string path1, string path2) {
		string new_path = "";
		Path old_path_1(path1);
		Path old_path_2(path2);

		for (int i = 0; i < old_path_1.return_size_element(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += old_path_1.return_path_element(i);
		}
		for (int i = 0; i < old_path_2.return_size_element(); i++) {
			new_path += "\\";
			new_path += old_path_2.return_path_element(i);
		}
		return new_path;
	}
};
int main() {
	//a
	Path path("D:\\local\\app.exe");
	if(path.path_is_file()){
		cout << "Path is a file\n\n";
	}
	else{
		cout << "Path is not a file";
	}
	//b
	Path path1("C:\\user\\programs\\\"Fire fox\"\\firefox.exe");
	cout <<"\n"<<path1.return_file_name(false);
	cout << "\n" << path1.return_file_name();
	cout << "\n\n" << path1.return_path();
	//c
	Path obj1 = Path("D:\\programs\\google\\");
	Path obj2 = Path("revel\\google.exe");
	Path merged = merged.merge_path(obj1, obj2);
	cout << "\n\n" << merged.return_path();
	
	cout <<'\n'<< merged.return_path_element(3);
	
	//  exception
	cout << "\n\n";
	Path path0("C:\\user\\progr<ams\\\"Fire fox\"\\firefox.exe");

	cout << '\n' << endl;
	return 0;
}
