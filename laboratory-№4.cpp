#include <iostream>
#include <vector>
#include <string>
using namespace std;

class String_list
{
public:
	String_list();

	~String_list();

	int get_size()
	{
		return size;
	}

	void push_back(string data)
	{
		if (head == nullptr)
		{
			head = new Node(data);
		}
		else
		{
			Node* current = this->head;

			while (current->next != nullptr)
			{
				current = current->next;
			}

			current->next = new Node(data);
		}

		size++;
	}

	string& operator[](const int index)
	{
		int count = 0;
		Node* current = this->head;

		while (current != nullptr)
		{
			if (count == index)
			{
				return current->data;
			}
			else
			{
				current = current->next;
				count++;
			}
		}
	}

	void pop_front()
	{
		Node* temp = head;
		head = head->next;

		delete temp;

		size--;
	}

	void clear()
	{
		while (size)
		{
			pop_front();
		}
	}

private:

	class Node
	{
	public:

		Node* next;
		string data;

		Node(string data = "", Node* next = nullptr)
		{
			this->data = data;
			this->next = next;
		}
	};

	int size;

	Node* head;
};

String_list::String_list()
{
	size = 0;
	head = nullptr;
}

String_list::~String_list()
{

}

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Path {
	vector<string> path_elements;
	const string EXCEPTIONS = "*/?<>";

	bool path_has_disk = false;
	bool path_has_file = false;
	int length_file_extention = 0;

public:

	Path(string path) {
		try {
			if (!ext(path)) {
				throw 1;
			}
			check_path(path);
		}
		catch (int ex) {
			cout << "\nError: " << ex << endl;
			cout << "Cant read path - " << path << endl;
		}
	}

	Path(String_list path_list)
	{
		list_to_path(path_list);
	}

private:

	void list_to_path(String_list path_list)
	{
		try {
			for (int i = 0; i < path_list.get_size(); i++) {
				if (!ext(path_list[i]))
					throw 1;

				for (int j = 0; j < path_list[i].size(); j++)
				{
					if (path_list[i][j] == ':')
					{
						path_has_disk = true;
					}
					else if (path_list[i][j] == '.')
					{
						path_has_file = true;
						length_file_extention = j;
					}
				}
				path_elements.push_back(path_list[i]);
			}
		}
		catch (int ex)
		{
			cout << "\nError: " << ex << endl;
			cout << "Cant read path list"<< endl;
		}
	}

	bool ext(string path)
	{
		for (int i = 0; i < path.size(); i++)
		{
			for (int j = 0; j < EXCEPTIONS.size(); j++)
			{
				if (EXCEPTIONS[j] == path[i])
				{
					return false;
				}
			}
		}
		return true;
	}

	void check_path(string path) {
		int substr_flag = 0;
		int delta = 0;

		for (int i = 0; i < path.length(); i++) {
			if ((path[i] == '\\') && (path[i - 1] == ':') && (path_elements.size() == 0)) {

				path_has_disk = true;

				substr_flag = i + 1;
				path_elements.push_back(path.substr(0, i));
			}
			else if (path[i] == '\\' && (i - substr_flag) != 0) {
				delta = i - substr_flag;
				path_elements.push_back(path.substr(substr_flag, delta));
				substr_flag = i + 1;
			}
			else if (path[i] == '.') {
				delta = path.size() - substr_flag;

				length_file_extention = delta - (path.size() - i);
				path_has_file = true;

				path_elements.push_back(path.substr(substr_flag, delta));
				substr_flag = i;
			}
		}
		if (path_elements.size() == 0) {
			cout << "Can\'t read >> " << path << " << path.";
		}
	}

	string build_path() {
		string path = "";
		for (int i = 0; i < path_elements.size(); i++) {
			if (i != 0) {
				path += "\\";
			}
			path += path_elements[i];
		}
		return path;
	}

public:
	string return_file_name(bool full_name = true) { //false - сли нужно вывести название файла без расширения
		return (path_has_file) && (full_name) ? path_elements[path_elements.size() - 1] : path_has_file ? path_elements[path_elements.size() - 1].substr(0, length_file_extention) : "";
	}

	string return_path_element(int element_index) {
		return path_elements[element_index];
	}

	string return_path() {
		return build_path();
	}

	bool path_is_file() {
		if (path_has_file)
			return true;
		else
			return false;

	}

	int return_size_element() {
		return path_elements.size();
	}

	Path merge_path(Path path1, Path path2) {
		string new_path = "";

		for (int i = 0; i < path1.path_elements.size(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += path1.path_elements[i];
		}
		for (int i = 0; i < path2.path_elements.size(); i++) {
			new_path += "\\";
			new_path += path2.path_elements[i];
		}
		return new_path;
	}

	string merge_path(string path1, string path2) {
		string new_path = "";
		Path old_path_1(path1);
		Path old_path_2(path2);

		for (int i = 0; i < old_path_1.return_size_element(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += old_path_1.return_path_element(i);
		}
		for (int i = 0; i < old_path_2.return_size_element(); i++) {
			new_path += "\\";
			new_path += old_path_2.return_path_element(i);
		}
		return new_path;
	}
	
	Path& operator = (const Path& other) {
		if (this == &other) {
			return *this;
		}

		path_has_disk = other.path_has_disk;
		path_has_file = other.path_has_file;
		length_file_extention = other.length_file_extention;
		path_elements = other.path_elements;
		return *this;
	}

	Path operator + (const Path& other) {
		Path temp = temp.merge_path(*this, other);
		return temp;
	}

	Path operator += (const Path& other) {
		Path temp = temp.merge_path(*this, other);
		*this = temp;
		return *this;
	}

	string& operator[](int index) { // & !!
		try {
			if (index > path_elements.size() || index < 0)
				throw 2;
			return path_elements[index];
		}
		catch (int ex) {
			cerr << "ERROR: " << ex << endl;
			cerr << "Index out of range" << endl;
		}
	}

	bool operator == (const Path& other) {
		if (path_elements.size() == other.path_elements.size()) {
			if (path_has_disk == other.path_has_disk)
				if (path_has_file == other.path_has_file)
					if (length_file_extention == other.length_file_extention) {

						for (int i = 0; i < path_elements.size(); i++) {
							if (path_elements[i] != other.path_elements[i]) {
								return false;
							}
						}
						return true;
					}
		}
		else
			return false;
	}

	bool operator != (const Path& other) {
		if (*this == other) {
			return false;
		}
		else {
			return true;
		}
	}

	bool operator == (string str) {
		if (return_path() == str) {
			return true;
		}
		else {
			return false;
		}
	}

	bool operator != (string str) {
		if (return_path() != str) {
			return true;
		}
		else {
			return false;
		}
	}

	friend ostream& operator << (ostream& stream, const Path& other);
};

ostream& operator << (ostream& stream, const Path& other) {
	for (int i = 0; i < other.path_elements.size(); i++) {
		if (i != 0) {
			stream << "\\";
		}
		stream << other.path_elements[i];
	}
	return stream;
}


int main()
{

	String_list path_list;
	path_list.push_back("C:");
	path_list.push_back("aaa");
	path_list.push_back("bbb");
	path_list.push_back("ccc");
	path_list.push_back("gg.exe");

	Path path(path_list);

	cout << path << endl;
	cout << path.return_file_name() << endl;
	cout << path.return_file_name(false) << endl;


	return 0;
}