#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <random>
using namespace std;

class Path {
	vector<string> path_elements;
	const string EXCEPTIONS = "*/?<>";

	bool path_has_disk = false;
	bool path_has_file = false;
	int length_file_extention = 0;

public:

	Path(string path) {
		if (!ext(path)) {
			throw exception("Can't read path");
		}
		check_path(path);
		
	}

	Path(){}

private:

	bool ext(string path)
	{
		for (int i = 0; i < path.size(); i++)
		{
			for (int j = 0; j < EXCEPTIONS.size(); j++)
			{
				if (EXCEPTIONS[j] == path[i])
				{
					return false;
				}
			}
		}
		return true;
	}

	void check_path(string path) {
		int substr_flag = 0;
		int delta = 0;

		for (int i = 0; i < path.length(); i++) {
			if ((path[i] == '\\') && (path[i - 1] == ':') && (path_elements.size() == 0)) {

				path_has_disk = true;

				substr_flag = i + 1;
				path_elements.push_back(path.substr(0, i));
			}
			else if (path[i] == '\\' && (i - substr_flag) != 0) {
				delta = i - substr_flag;
				path_elements.push_back(path.substr(substr_flag, delta));
				substr_flag = i + 1;
			}
			else if (path[i] == '.') {
				delta = path.size() - substr_flag;

				length_file_extention = delta - (path.size() - i);
				path_has_file = true;

				path_elements.push_back(path.substr(substr_flag, delta));
				substr_flag = i;
			}
		}
		if (path_elements.size() == 0) {
			//throw "Can't read path";
		}
	}

	string build_path() {
		string path = "";
		for (int i = 0; i < path_elements.size(); i++) {
			if (i != 0) {
				path += "\\";
			}
			path += path_elements[i];
		}
		return path;
	}

public:

	string return_file_name(bool full_name = true) { //false - сли нужно вывести название файла без расширения
		return (path_has_file) && (full_name) ? path_elements[path_elements.size() - 1] : path_has_file ? path_elements[path_elements.size() - 1].substr(0, length_file_extention) : "";
	}

	string return_path_element(int element_index) {
		return path_elements[element_index];
	}

	string return_path() {
		return build_path();
	}

	bool path_is_file() {
		if (path_has_file)
			return true;
		else
			return false;

	}

	int return_size_element() {
		return path_elements.size();
	}

	Path merge_path(Path path1, Path path2) {
		string new_path = "";

		for (int i = 0; i < path1.path_elements.size(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += path1.path_elements[i];
		}
		for (int i = 0; i < path2.path_elements.size(); i++) {
			new_path += "\\";
			new_path += path2.path_elements[i];
		}
		return new_path;
	}

	string merge_path(string path1, string path2) {
		string new_path = "";
		Path old_path_1(path1);
		Path old_path_2(path2);

		for (int i = 0; i < old_path_1.return_size_element(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += old_path_1.return_path_element(i);
		}
		for (int i = 0; i < old_path_2.return_size_element(); i++) {
			new_path += "\\";
			new_path += old_path_2.return_path_element(i);
		}
		return new_path;
	}

	Path& operator = (const Path& other) {
		if (this == &other) {
			return *this;
		}

		path_has_disk = other.path_has_disk;
		path_has_file = other.path_has_file;
		length_file_extention = other.length_file_extention;
		path_elements = other.path_elements;
		return *this;
	}

	Path& operator = (string path)
	{
		Path new_path(path);
		this->path_elements = new_path.path_elements;
		return *this;
	}


	Path operator + (const Path& other) {
		Path temp = temp.merge_path(*this, other);
		return temp;
	}

	Path operator += (const Path& other) {
		Path temp = temp.merge_path(*this, other);
		*this = temp;
		return *this;
	}

	string& operator[](int index) { // & !!
		try {
			if (index > path_elements.size() || index < 0)
				throw 2;
			return path_elements[index];
		}
		catch (int ex) {
			cerr << "ERROR: " << ex << endl;
			cerr << "Index out of range" << endl;
		}
	}

	bool operator == (const Path& other) {
		if (path_elements.size() == other.path_elements.size()) {
			if (path_has_disk == other.path_has_disk)
				if (path_has_file == other.path_has_file)
					if (length_file_extention == other.length_file_extention) {

						for (int i = 0; i < path_elements.size(); i++) {
							if (path_elements[i] != other.path_elements[i]) {
								return false;
							}
						}
						return true;
					}
		}
		else
			return false;
	}

	bool operator != (const Path& other) {
		if (*this == other) {
			return false;
		}
		else {
			return true;
		}
	}

	bool operator == (string str) {
		if (return_path() == str) {
			return true;
		}
		else {
			return false;
		}
	}

	bool operator != (string str) {
		if (return_path() != str) {
			return true;
		}
		else {
			return false;
		}
	}

	friend ostream& operator << (ostream& stream, const Path& other);
};

ostream& operator << (ostream& stream, const Path& other) {
	for (int i = 0; i < other.path_elements.size(); i++) {
		if (i != 0) {
			stream << "\\";
		}
		stream << other.path_elements[i];
	}
	return stream;
}

class Bad_file
{
public:

	Bad_file(){}

	Bad_file(Path path)
	{
		file.open(path.return_path());

		if (!file.is_open())
		{
			throw exception("Can't open file ");
		}
	}

	Bad_file(string path)
	{
		file.open(path);

		if (!file.is_open())
		{
			throw exception("Can't open file ");
		}
	}

	string broke(string str)
	{
		char temp;
		for (int i = 0; i < str.size(); i++)
		{
			str[i] += random(255);
		}
		return str;
	}

	void replace()
	{
		int flag_old = 0;
		int flag_new = 0;

		string buffer = "";
		while(file >> buffer)
		{
			flag_old = flag_new;
			flag_new = file.tellg();
			file.seekg(flag_old);
			file << broke(buffer) << endl;
			file.seekg(flag_new);
		}
	}

	~Bad_file()
	{
		file.close();
	}

	Bad_file& operator = (string path)
	{
		this->file.open(path);
		if (!file.is_open())
		{
			throw exception("Can't open file ");
		}
		return *this;
	}

private:
	fstream file;

	int random(int n)
	{
		return rand() % n;
	}
};

void bad()
{

	ifstream bad("bad.txt");
	string temp_path;
	vector<Path> paths;
	for(int i = 0; bad >> temp_path; i++)
	{
		paths.push_back(temp_path);
	}
	
	Bad_file* bad_file = new Bad_file[paths.size()];
	for(int i = 0; i < paths.size(); i++)
	{
		bad_file[i] = paths[i].return_path();
		bad_file[i].replace();
	}
}

int main(){
	try
	{
		bad();

		return 0;
	}
	catch (exception &ex)
	{
		cout << "Error: " << ex.what() << endl;
		Bad_file bad_file("bad.txt");
		bad_file.replace();
	}
}