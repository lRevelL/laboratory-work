#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Path
{
public:
	
	bool operator == (string other)
	{
		string str_path = "";
		for (int i = 0; i < this->path.size(); i++)
		{
			str_path += this->path[i];
		}
		if (str_path == other)
			return true;
		else
			return false;
	}

	bool operator != (string other)
	{
		string str_path = "";
		for (int i = 0; i < str_path.size(); i++)
		{
			str_path += this->path[i];
		}
		if (str_path != other)
			return true;
		else
			return false;
	}

	virtual Path& operator = (string) = 0;

	virtual bool ext(string) = 0;

protected:

	vector<string> path;

	virtual void check_path(string) = 0;

	friend ostream& operator << (ostream& stream, const Path& other);

};

ostream& operator << (ostream& stream, const Path& other)
{

	for (int i = 0; i < other.path.size(); i++)
	{

		stream << other.path[i];

	}

	return stream;
}

class Win_path : public Path
{
public:

	Win_path() {}

	Win_path(string path)
	{
		try {
			if (ext(path))
				check_path(path);
			else
				throw 1;
		}
		catch(int e)
		{
			cerr << "\nERROR:" << e << " Bad symbol in the path" << endl;
		}
	}

	Path& operator = (string path) override
	{
		Win_path new_path(path);
		this->path = new_path.path;
		return *this;
	}

private:

	const string EXT = "*?^/<>|";

	bool ext(string path) override
	{
		for (int i = 0; i < path.size(); i++)
		{
			for (int j = 0; j < EXT.size(); j++)
			{
				if (EXT[j] == path[i])
				{
					return false;
				}
			}
		}
		return true;
	}

	void check_path(string path) override
	{
		int substr_flag = 0;
		int delta = 0;

		for (int i = 0; i < path.size(); i++)
		{
			if ((path[i] == '\\') && (path[i - 1] == ':'))
			{
				substr_flag = i;

				this->path.push_back(path.substr(0, i));

			}
			else if (path[i] == '\\' && (i - substr_flag) != 0)
			{
				delta = i - substr_flag;

				this->path.push_back(path.substr(substr_flag, delta));

				substr_flag = i;

			}
			else if (path[i] == '.')
			{
				delta = path.size() - substr_flag;

				this->path.push_back(path.substr(substr_flag, delta));

				substr_flag = i;
			}
		}
	}


};

class Linux_path : public Path
{
public:

	Linux_path() {}

	Linux_path(string path)
	{
		try 
		{
			if (ext(path))
				check_path(path);
			else
				throw 1;
		}
		catch (int e)
		{
			cerr << "ERROR:" << e << " Bad symbol in the path";
		}
	}

	Path& operator = (string path) override
	{
		Linux_path new_path(path);
		this->path = new_path.path;
		return *this;
	}

private:

	const string EXT = "!@#$&~%*()[]{}\'\":;><`";

	bool ext(string path) override
	{
		for (int i = 0; i < path.size(); i++)
		{
			for (int j = 0; j < EXT.size(); j++)
			{
				if (EXT[j] == path[i])
				{
					return false;
				}
			}
		}
		return true;
	}

	void check_path(string path) override
	{
		int substr_flag = 0;
		int delta = 0;

		for (int i = 0; i < path.size(); i++)
		{
			if (path[i] == '/' && (i - substr_flag) != 0 && (i != 0))
			{
				delta = i - substr_flag;

				this->path.push_back(path.substr(substr_flag, delta));

				substr_flag = i;

			}
			else if (path[i] == '.')
			{
				delta = path.size() - substr_flag;

				this->path.push_back(path.substr(substr_flag, delta));

				substr_flag = i;
			}
		}
	}

};


int main()
{
	Path* path[3];

	path[0] = new Win_path;
	path[1] = new Linux_path("home/Nikita/Visual/setup.exe");
	path[2] = new Win_path;

	string bad_path = "D:\\Laba-3\\*source.cpp";
	if (!(path[0]->ext(bad_path)))
		cout << bad_path <<" is bad path" << endl;

	cout << '\n' << "*path[1] = " << *path[1] << endl;;

	*path[0] = "E:\\aaaa\\bbbb.exe";
	*path[1] = "home/Nikita/Program File/Visual/logs/2019-10-12/logs125323.txt";
	*path[2] = "G:\\rrrrr\\hhhhhh";

	cout << '\n' << "*path[0] = " << *path[0] << endl;
	cout << "*path[1] = " << *path[1] << endl;
	cout << "*path[2] = " << *path[2] << endl;

	if (*path[0] == "E:\\aaaa\\bbbb.exe")
		cout << '\n' << "*path[0] == E:\\aaaa\\bbbb.exe" << endl;

	if (*path[1] != "E:\\aaaa\\bbbb.exe")
		cout << "*path[1] != E:\\aaaa\\bbbb.exe" << endl;

	return 0;
}