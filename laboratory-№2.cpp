#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Path {
	vector<string> path_elements;
	const string EXCEPTIONS = "*/?<>";

	bool path_has_disk = false;
	bool path_has_file = false;
	int length_file_extention = 0;

public:
	Path(string path) {
		try{
			if(path_has_exceptions(path)){
				throw 1;
			}
				check_path(path);
		}
			catch (int ex){
				cout << "\nError: " << ex << endl;
				cout << "Cant read path - " << path  << endl;
			}
	}

private:
	
	bool path_has_exceptions(string path){
		int i = 0;
		while (i < path.length()){
			i ++;
			if (path_has_exception_symbol(path[i])) {
				return true;
			}
		}
		return false;
	}
	bool path_has_exception_symbol(char symbol) {
		for (int i = 0; i < EXCEPTIONS.size(); i++) {
			if (symbol == EXCEPTIONS[i]) {
				return true;
			}
		}
		return false;
	}

	void check_path(string path) {
		int substr_flag = 0; //ôëàã ñðåçà
		int delta = 0;//ðàçíèöà ìåæäó ïðåäûäóùèì ôëàãîì è òåêóùèì
		
			for (int i = 0; i < path.length(); i++) {//ïîèñê äèñêà
				if ((path[i] == '\\') && (path[i - 1] == ':') && (path_elements.size() == 0)) {

					path_has_disk = true;

					substr_flag = i + 1;
					path_elements.push_back(path.substr(0, i)); //äîáàâëåíèå äèñêà â âåêòîð
					//cout << "-Found disk: " << path.substr(0, i) << endl;
				}
				else if (path[i] == '\\' && (i - substr_flag) != 0) { //ïîèñê ïàïîê
					delta = i - substr_flag;
					path_elements.push_back(path.substr(substr_flag, delta)); //äîáàâëåíèå ïàïêè â âåêòîð
					//cout << "--Found folder: " << path.substr(substr_flag, delta) << endl;
					substr_flag = i + 1;
				}
				else if (path[i] == '.') {//ïîèñê ôàéëà
					delta = path.size() - substr_flag;

					length_file_extention = delta - (path.size() - i);
					path_has_file = true;

					path_elements.push_back(path.substr(substr_flag, delta)); //äîáàâëåíèå ôàéëà â âåêòîð
					//cout << "---Found file: " << path.substr(substr_flag, delta) << endl;
					substr_flag = i;
				}
			}
			if (path_elements.size() == 0) { //ïðîâåðêà íà êîððåêòíîñòü ïóòè
				cout << "Can\'t read >> " << path << " << path.";//íåêîððåêòåí
			}
	}


	string build_path() {
		string path = "";
		for (int i = 0; i < path_elements.size(); i++) {
			if (i != 0) {
				path += "\\";
			}
			path += path_elements[i];
		}
		return path;
	}
	
public:
	string return_file_name(bool full_name = true) { //false - ñëè íóæíî âûâåñòè íàçâàíèå ôàéëà áåç ðàñøèðåíèÿ
		return (path_has_file) && (full_name) ? path_elements[path_elements.size() - 1] : path_has_file ? path_elements[path_elements.size() - 1].substr(0, length_file_extention) : "";
	}

	string return_path_element(int element_index) {
		return path_elements[element_index];
	}

	string return_path() {
		return build_path();
	}
	bool path_is_file(){
		if(path_has_file)
			return true;
		else
			return false;
		
	}
	int return_size_element() {
		return path_elements.size();
	}

	Path merge_path(Path path1, Path path2) {
		string new_path = "";

		for (int i = 0; i < path1.path_elements.size(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += path1.path_elements[i];
		}
		for (int i = 0; i < path2.path_elements.size(); i++) {
			new_path += "\\";
			new_path += path2.path_elements[i];
		}
		return new_path;
	}

	string merge_path(string path1, string path2) {
		string new_path = "";
		Path old_path_1(path1);
		Path old_path_2(path2);

		for (int i = 0; i < old_path_1.return_size_element(); i++) {
			if (i != 0) {
				new_path += "\\";
			}
			new_path += old_path_1.return_path_element(i);
		}
		for (int i = 0; i < old_path_2.return_size_element(); i++) {
			new_path += "\\";
			new_path += old_path_2.return_path_element(i);
		}
		return new_path;
	}
	//operator =
	Path& operator = (const Path& other){
		if (this == &other){
			return *this;
		}
		
		path_has_disk = other.path_has_disk;
		path_has_file = other.path_has_file;
		length_file_extention = other.length_file_extention;
		path_elements = other.path_elements;
		return *this;
	}
	//operator +
	Path operator + (const Path& other){
		Path temp = temp.merge_path(*this,other);
		return temp;
	}
	//operator +=
	Path operator += (const Path& other){
		Path temp = temp.merge_path(*this,other);
		*this = temp;
		return *this;
	}
	//operator []
	string & operator[](int index){ // & !!
		try{
			if (index > path_elements.size() || index < 0)
			throw 2;
			return path_elements[index];
		}
		catch (int ex){
			cerr << "ERROR: " << ex << endl;
			cerr << "Index out of range" << endl;
		}
	}
	//operator ==, !=
	bool operator == (const Path & other){
		if (path_elements.size() == other.path_elements.size()){
		if (path_has_disk == other.path_has_disk)
		if (path_has_file == other.path_has_file)
		if (length_file_extention == other.length_file_extention){
		
			for(int i = 0; i < path_elements.size(); i++){
				if (path_elements[i] != other.path_elements[i]){
					return false;
				}
			}
			return true;
		}
		}
		else
			return false;
	}
	
	bool operator != (const Path & other){
		if(*this == other){
			return false;
		}
		else{
			return true;
		}
	}
	bool operator == (string str){
		if (return_path() == str){
			return true;
		}
		else{
			return false;
		}
	}
	bool operator != (string str){
		if (return_path() != str){
			return true;
		}
		else{
			return false;
		}
	}
	
	friend ostream &operator << (ostream &stream,const Path &other);
};

ostream &operator << (ostream &stream,const Path &other){
	for (int i = 0; i < other.path_elements.size(); i++) {
		if (i != 0) {
			stream <<"\\";
		}
		stream << other.path_elements[i];
	}
	return stream;
}

int main() {

	//operator =
	Path path21("F:\\user\\Nikita\\desktop");
	cout << "path21 = " <<path21;
	Path path22("\\hero\\LTT-154\\");
	cout << "\n" << "path22 = " << path22;
	cout << "\npath22 = 21";
	path22 = path21;
	cout << "\n" << "path21 = " << path21;
	cout << "\n" << "path22 = " << path22 << "\n\n";
	
	//operator +
	Path path23("D:\\Program\\");
	Path path24("cheburechnaya\\cheburek.com");
	Path path25 = path23+path24;
	cout << path25 << endl;
	
	//operator +=
	path23 += path24;
	cout << path23 <<"\n\n";
	
	//operator []
	cout << path23[2] << endl;
	cout << '\n' << endl;
	
	//operator ==, !=
	if (path25 == path23){
		cout << "path25 equal path23\n";
	}
	else
		cout << "path25 not equal path23\n";
	if (path25 != path24){
		cout << "path25 not equal path24\n";
	}
	else
		cout << "path25 equal path24\n";

	return 0;
}